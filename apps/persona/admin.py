from django.contrib import admin

from apps.persona.models import Perfil
from apps.persona.models import Persona

admin.site.register(Persona)
admin.site.register(Perfil)
