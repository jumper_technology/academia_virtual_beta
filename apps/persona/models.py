# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User


from vacademy.choices import STATUS_CHOICES, PROFILE_CHOICES, GENDER_CHOICES



class Persona(models.Model):
    '''
    Todos los usuarios en general se relacionan a una persona para la
    informacion general
    '''
    dni = models.CharField(u'D.N.I.', max_length=8, unique=True, null= True, blank= True)
    nombres = models.CharField(u'Nombres', max_length=150)
    apellido_paterno = models.CharField(u'Apellido paterno', max_length=100)
    apellido_materno = models.CharField(u'Apellido materno', max_length=100)
    correo = models.EmailField(u'Correo Electronico', max_length=254, null= True, blank= True)
    telefono = models.CharField(u'Número telefónico', max_length=50, null= True, blank= True)
    genero = models.CharField(u'Genero', max_length=10, choices=GENDER_CHOICES, default='F')
    estado_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_creacion = models.ForeignKey(User, related_name='+')
    usuario_modificacion = models.ForeignKey(User, related_name='+', null= True, blank= True)

    def __unicode__(self):
        return u'%s %s, %s' % (
                                self.apellido_paterno.capitalize(),
                                self.apellido_materno.capitalize(),
                                self.nombres.capitalize()
                                )

    class Meta:
        ordering = ['apellido_paterno']
        verbose_name = u'Persona'
        verbose_name_plural = u'Personas'



class Perfil(models.Model):
    '''
    Las personas que haran uso del sistema tendran afiliado al menos un perfil,
    donde se asignara los permisos segun el perfil que lleva.
    '''
    usuario = models.OneToOneField(User)
    persona = models.OneToOneField('Persona')
    tipo_perfil = models.CharField(max_length=7, choices=PROFILE_CHOICES, default='NORMAL')
    estado_id = models.CharField(u'Estado', max_length=10, choices=STATUS_CHOICES, default='A', db_index=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_creacion = models.ForeignKey(User, related_name='+')
    usuario_modificacion = models.ForeignKey(User, related_name='+', null= True, blank= True)

    def __unicode__(self):
        return u'Perfil de %s' % (self.usuario.username)


    class Meta:
        ordering = ['persona']
        verbose_name = u'Perfil'
        verbose_name_plural = u'Perfiles'
