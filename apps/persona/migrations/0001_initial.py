# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-23 05:47
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo_perfil', models.CharField(choices=[('N', 'Normal'), ('E', 'Alumno'), ('C', 'Docente'), ('T', 'Administrador')], default='NORMAL', max_length=7)),
                ('estado_id', models.CharField(choices=[('A', 'Activo'), ('I', 'Inactivo')], db_index=True, default='A', max_length=10, verbose_name='Estado')),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['persona'],
                'verbose_name': 'Perfil',
                'verbose_name_plural': 'Perfiles',
            },
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dni', models.CharField(blank=True, max_length=8, null=True, unique=True, verbose_name='D.N.I.')),
                ('nombres', models.CharField(max_length=150, verbose_name='Nombres')),
                ('apellido_paterno', models.CharField(max_length=100, verbose_name='Apellido paterno')),
                ('apellido_materno', models.CharField(max_length=100, verbose_name='Apellido materno')),
                ('correo', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Correo Electronico')),
                ('telefono', models.CharField(blank=True, max_length=50, null=True, verbose_name='Número telefónico')),
                ('genero', models.CharField(choices=[('M', 'Masculino'), ('F', 'Femenino')], default='F', max_length=10, verbose_name='Genero')),
                ('estado_id', models.CharField(choices=[('A', 'Activo'), ('I', 'Inactivo')], db_index=True, default='A', max_length=10, verbose_name='Estado')),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('usuario_creacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('usuario_modificacion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['apellido_paterno'],
                'verbose_name': 'Persona',
                'verbose_name_plural': 'Personas',
            },
        ),
        migrations.AddField(
            model_name='perfil',
            name='persona',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='persona.Persona'),
        ),
        migrations.AddField(
            model_name='perfil',
            name='usuario',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='perfil',
            name='usuario_creacion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='perfil',
            name='usuario_modificacion',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
    ]
