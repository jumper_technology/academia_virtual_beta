import { FrontdeskPage } from './app.po';

describe('frontdesk App', () => {
  let page: FrontdeskPage;

  beforeEach(() => {
    page = new FrontdeskPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
