import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { IndexComponent } from '../index/index.component';
import { HomePlansComponent } from '../index/plans.component';
import { EtaComponent } from '../eta/eta.component';
import { EtaCreationComponent } from '../eta/creation.component';
import { PaymentComponent } from '../payment/payment.component';
import { PricesComponent } from '../prices/prices.component';

const routes: Routes = [
    { path: '', component: IndexComponent },
    { path: 'home/planes', component: HomePlansComponent },
    { path: 'home/prices', component: IndexComponent },
    { path: 'home/prices', component: IndexComponent },
    { path: 'eta/nuevo', component: EtaCreationComponent },
    { path: 'eta', component: EtaComponent },
    { path: 'pagos', component: PaymentComponent },
    { path: 'precios', component: PricesComponent },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
