import { Component, OnInit } from '@angular/core';
import { Eta } from './eta';
import { EtaCategory } from './eta-category';
import { EtaQuestion } from './eta-question';

@Component({
  selector: 'app-eta',
  templateUrl: './eta.component.html',
  styleUrls: ['./eta.component.css']
})
export class EtaComponent implements OnInit {
  eta: Eta;
  cat_index: number = 0;
  que_index: number = 0;
  current_category: EtaCategory;
  current_question: EtaQuestion;
  answer_choices: Array<string> = ['a', 'b', 'c', 'd', 'e'];
  constructor() { }

  ngOnInit() {
    this.eta = new Eta('Examen demo', 120, '', new Date(), new Date(), [
      new EtaCategory('mates', 'bla', [
        new EtaQuestion('pregunta larga1', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga2', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga3', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga4', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga5', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga6', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga7', ['a','b','c', 'd', 'e'], NaN),
      ]),
      new EtaCategory('lenguaje', 'bla', [
        new EtaQuestion('pregunta larga8', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga9', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga10', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga11', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga12', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga13', ['a','b','c', 'd', 'e'], NaN),
      ]),
      new EtaCategory('biologia', 'bla', [
        new EtaQuestion('pregunta larga14', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga15', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga16', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga17', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga18', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga19', ['a','b','c', 'd', 'e'], NaN),
      ]),
      new EtaCategory('fisica', 'bla', [
        new EtaQuestion('pregunta larga20', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga21', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga22', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga23', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga24', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga25', ['a','b','c', 'd', 'e'], NaN),
      ]),
      new EtaCategory('loquesea', 'bla', [
        new EtaQuestion('pregunta larga26', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga27', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga28', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga29', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga30', ['a','b','c', 'd', 'e'], NaN),
        new EtaQuestion('pregunta larga31', ['a','b','c', 'd', 'e'], NaN),
      ]),
    ]);
    this.current_category = this.eta.categories[0];
    this.current_question = this.current_category.questions[0];
  }

  next(): void {
    this.que_index++;
    if( this.que_index < this.current_category.questions.length ){
      this.current_question = this.current_category.questions[this.que_index];
    } else {
      this.que_index = 0;
      this.cat_index++;
      if( this.cat_index < this.eta.categories.length ) {
        this.current_category = this.eta.categories[this.cat_index];
        this.current_question = this.current_category.questions[this.que_index];
      } else {
        this.cat_index = 0;
        this.current_category = this.eta.categories[this.cat_index];
      }
    }
  }

  before(): void {
    console.log('before');
  }

  showAnswer(question: EtaQuestion): string {
    if(question){
      if(question.answer >= 0 && question.answer <= 4 ){
          return this.answer_choices[question.answer] || '-';
      }
    }
    return '-';
  }
}
