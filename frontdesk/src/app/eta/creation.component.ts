import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Eta } from './eta';
import { EtaCategory } from './eta-category';
import { EtaQuestion } from './eta-question';

@Component({
  selector: 'eta-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./eta.component.css']
})
export class EtaCreationComponent implements OnInit {
  eta: Eta;
  state: number = 1;
  etaModel: any = {};
  qst: any = {};

  constructor() { }

  ngOnInit() {
    //REM
    this.eta = new Eta('Examen admisión UNI',180, '',new Date(), new Date(), []);
  }

  saveEta(model: any):void {
    this.eta = new Eta(model.name, +model.resolutionTime, model.description,
                  new Date(model.openAt), new Date(model.closeAt), []);
  }

  addCategory(name: string): void {
    let category = new EtaCategory(name, '', []);
    this.eta.categories.push(category);
  }

  addQuestion(category: EtaCategory ,q: any): void {
    let answers: Array<string> = [q.answerA, q.answerB, q.answerC, q.answerD, q.answerE]
    let question: EtaQuestion = new EtaQuestion(q.name, answers, +q.answer);
    category.questions.push(question);
    this.qst = {};
    (<any>category).add = false;
  }

}
