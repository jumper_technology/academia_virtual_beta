import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Eta } from './eta';
import { EtaCategory } from './eta-category';
import { EtaQuestion } from './eta-question';

@Component({
  selector: 'eta-question',
  templateUrl: './question.component.html',
  styleUrls: ['./eta.component.css']
})
export class EtaQuestionComponent implements OnInit {
  @Input()question: EtaQuestion;
  @Output()questionChange = new EventEmitter();
  @Input()category: string;
  @Input()index: number;

  constructor() { }

  ngOnInit() {

  }
  change(index: number) {
    this.question.answer = index;
    this.questionChange.emit(this.question);
  }
}
