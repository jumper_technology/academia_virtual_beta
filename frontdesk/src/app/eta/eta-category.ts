import { EtaQuestion } from './eta-question';

export class EtaCategory {
  constructor(
    public name: string,
    public description: string,
    public questions: Array<EtaQuestion>
  ) {}
};
