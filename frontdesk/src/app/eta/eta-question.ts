
export class EtaQuestion {
  constructor(
    public description: string,
    public alternatives: Array<string>,
    public answer: number
  ) {}
};
