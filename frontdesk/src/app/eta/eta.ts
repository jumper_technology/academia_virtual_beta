import { EtaCategory } from './eta-category';
export class Eta {

  constructor(
    public name: string,
    public resolutionTime: number,
    public description: string,
    public openAt: Date,
    public closeAt: Date,
    public categories: Array<EtaCategory>
  ) { }
};
