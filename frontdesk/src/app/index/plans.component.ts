import { Component, OnInit } from '@angular/core';
import { Plan } from './plan';

@Component({
  selector: 'home-plans',
  templateUrl: './plans.component.html',
})
export class HomePlansComponent implements OnInit {
  plans: Array<Plan>;

  constructor() { }

  ngOnInit() {
    this.plans = [
      new Plan('escolar', 'educación basica', 120, ''),
      new Plan('pre universitario', 'listo para el exito', 160, ''),
      new Plan('E.T.A.s', 'retate a ti mismo', 200, ''),
      new Plan('Docente', 'de maestros para maestros', 80, ''),
    ];
  }

}
