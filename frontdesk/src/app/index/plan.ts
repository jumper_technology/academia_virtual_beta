export class Plan {

  constructor(
    public name: string,
    public description: string,
    public price: number,
    public imageUrl: string
  ) { }
};
