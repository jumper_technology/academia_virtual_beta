import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { HomePlansComponent } from './index/plans.component';
import { EtaComponent } from './eta/eta.component';
import { EtaQuestionComponent } from './eta/question.component';
import { EtaCreationComponent } from './eta/creation.component';
import { PaymentComponent } from './payment/payment.component';
import { PricesComponent } from './prices/prices.component';
// routes
import { AppRoutingModule } from './app-routing/app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    HomePlansComponent,
    EtaComponent,
    EtaQuestionComponent,
    EtaCreationComponent,
    PaymentComponent,
    PricesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
