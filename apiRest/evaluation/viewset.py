from django.shortcuts import get_object_or_404
from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route, api_view

from apps.evaluation.models import Alternative
from apps.evaluation.models import Question
from apps.evaluation.models import Eta
from apps.evaluation.models import EtaCategory
from apps.evaluation.models import EtaQuestion

from apiRest.evaluation.serializers import AlternativeSerializer
from apiRest.evaluation.serializers import QuestionSerializer
from apiRest.evaluation.serializers import EtaCategorySerializer
from apiRest.evaluation.serializers import EtaSerializer
from apiRest.evaluation.serializers import EtaNestedSerializer
from apiRest.evaluation.serializers import EtaCategorySerializer
from apiRest.evaluation.serializers import EtaQuestionSerializer



class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class AlternativeViewSet(viewsets.ModelViewSet):
    queryset = Alternative.objects.all()
    serializer_class = AlternativeSerializer


class EtaViewSet(viewsets.ModelViewSet):
    queryset = Eta.objects.all()
    serializer_class = EtaSerializer

    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        eta = get_object_or_404(self.queryset, pk=pk)
        serializer = EtaNestedSerializer(eta)
        return Response(serializer.data)

    @detail_route(methods=['get'])
    def categories(self, request, **kwargs):
        eta = self.get_object()
        eta_categories = EtaCategory.objects.filter(eta=eta.id)
        self.queryset = eta_categories
        self.serializer_class = EtaCategorySerializer

        serializer = EtaCategorySerializer(instance=self.queryset, many=True)
        return Response(serializer.data)


class EtaCategoryViewSet(viewsets.ModelViewSet):
    queryset = EtaCategory.objects.all()
    serializer_class = EtaCategorySerializer


class EtaQuestionViewSet(viewsets.ViewSet):
    queryset = Eta.objects.all()
    serializer_class = EtaQuestionSerializer

    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        eta = get_object_or_404(self.queryset, pk=pk)
        serializer = EtaNestedSerializer(eta)
        return Response(serializer.data)



@api_view(['GET'])
def data_json_eta(request,eta_id):
    eta = Eta.objects.get(id=eta_id).get_data_json()
    return Response(eta)
