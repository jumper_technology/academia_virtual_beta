from django.forms import widgets
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from django.contrib.auth.models import User
from apps.evaluation.models import Question
from apps.evaluation.models import Alternative
from apps.evaluation.models import Eta
from apps.evaluation.models import EtaCategory
from apps.evaluation.models import EtaQuestion


class AlternativeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alternative
        fields = (
            'question',
            'description',
        )
        read_only_fields = ('creation_date', 'update_date',
                            'creation_user', 'update_user',)


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = (
            'description',
            'answer',
            'type_question',
            'level',
            'university',
            'area',
            'course',
        )
        read_only_fields = ('creation_date', 'update_date',
                            'creation_user', 'update_user',)


class EtaQuestionSerializer(serializers.Serializer):
    alternatives = AlternativeSerializer(many=True, read_only=True)
    class Meta:
        model = Question
        fields = (
            'name',
            'alternatives'
        )
        # read_only_fields = ('update_date', 'creation_user', 'update_user',)


class EtaCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = EtaCategory
        fields = (
            'eta',
            'name',
            'description',
        )
        read_only_fields = ('update_date', 'creation_user', 'update_user',)


class EtaCategoryNestedSerializer(serializers.ModelSerializer):
    questions =  EtaQuestionSerializer(many=True, read_only=True)
    class Meta:
        model = EtaCategory
        fields = (
            'eta',
            'name',
            'description',
        )
        read_only_fields = ('update_date', 'creation_user', 'update_user',)


class EtaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Eta
        fields = (
            'id',
            'name',
            'resolution_time',
            'description',
            'open_at',
            'close_at',
        )
        read_only_fields = ('update_date', 'creation_user', 'update_user',)


class EtaNestedSerializer(serializers.ModelSerializer):
    categories = EtaCategorySerializer(many=True, read_only=True)
    class Meta:
        model = Eta
        fields = (
            'id',
            'name',
            'resolution_time',
            'description',
            'open_at',
            'close_at',
            'categories'
        )
        read_only_fields = ('update_date', 'creation_user',
                            'update_user', 'categories')
