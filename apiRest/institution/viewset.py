from django.shortcuts import render
from rest_framework import viewsets

from apps.institution.models import University
from apps.institution.models import Area
from apps.institution.models import Course

from apiRest.institution.serializers import UniversitySerializer
from apiRest.institution.serializers import AreaSerializer
from apiRest.institution.serializers import CourseSerializer



class UniversityViewSet(viewsets.ModelViewSet):
    queryset = University.objects.all()
    serializer_class = UniversitySerializer


class AreaViewSet(viewsets.ModelViewSet):
    queryset = Area.objects.all()
    serializer_class = AreaSerializer


class CourseViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
