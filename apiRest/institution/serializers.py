from django.forms import widgets
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from django.contrib.auth.models import User
from apps.evaluation.models import University
from apps.evaluation.models import Area
from apps.evaluation.models import Course


class UniversitySerializer(serializers.ModelSerializer):
    class Meta:
        model = University
        fields = (
            'name',
            'description',
        )
        read_only_fields = ('creation_date', 'update_date',
                            'creation_user', 'update_user',)


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = (
            'name',
            'description',
        )
        read_only_fields = ('creation_date', 'update_date',
                            'creation_user', 'update_user',)


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = (
            'name',
            'description',
        )
        read_only_fields = ('creation_date', 'update_date',
                            'creation_user', 'update_user',)
