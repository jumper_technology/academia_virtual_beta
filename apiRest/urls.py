# -*- coding: utf-8 -*-

from django.conf.urls import include
from django.conf.urls import url
from rest_framework import routers

from apiRest.persona.viewset import PersonaViewSet

from apiRest.evaluation.viewset import QuestionViewSet
from apiRest.evaluation.viewset import AlternativeViewSet
from apiRest.evaluation.viewset import EtaViewSet
from apiRest.evaluation.viewset import EtaCategoryViewSet
from apiRest.evaluation.viewset import EtaQuestionViewSet

from apiRest.institution.viewset import UniversityViewSet
from apiRest.institution.viewset import AreaViewSet
from apiRest.institution.viewset import CourseViewSet

from apiRest.evaluation.viewset import data_json_eta


router = routers.DefaultRouter()
router.register(r'personas', PersonaViewSet)
router.register(r'questions', QuestionViewSet)
router.register(r'alternatives', AlternativeViewSet)
router.register(r'institutions', UniversityViewSet)
router.register(r'areas', AreaViewSet)
router.register(r'courses', CourseViewSet)
router.register(r'etas', EtaViewSet)
router.register(r'eta_categories', EtaCategoryViewSet)
router.register(r'eta_questions', EtaQuestionViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^etas/data/(?P<eta_id>[a-z0-9]+)/$', data_json_eta, name='data_json_eta'),
]
