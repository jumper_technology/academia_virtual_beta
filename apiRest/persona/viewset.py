from django.shortcuts import render
from rest_framework import viewsets

from apps.persona.models import Persona
from apiRest.persona.serializers import PersonaSerializer



class PersonaViewSet(viewsets.ModelViewSet):
    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer
