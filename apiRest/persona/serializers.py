from django.forms import widgets
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from django.contrib.auth.models import User
from apps.persona.models import Persona
from apps.persona.models import Perfil


class PersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Persona
        fields = (
            'dni',
            'nombres',
            'apellido_paterno',
            'apellido_materno',
            'correo',
            'telefono',
            'genero',
        )
        read_only_fields = ('usuario_creacion', 'usuario_modificacion',
                            'fecha_creacion', 'fecha_modificacion',)
